import React, { Component } from "react";
import axios from "axios";

export default class Todo extends Component {
  constructor(props) {
    super(props);
    this.state = { todo: [{ name: "change" }] };
  }

  async componentDidMount() {
    const data = await axios.get("http://localhost:3001/todo");
    console.log(data);
    this.setState((this.state.todo = data.data));
  }

  render() {
    return <this.RenderList list={this.state.todo}></this.RenderList>;
  }

  RenderList(list = []) {
    list = list.list;
    return list.map((_item) => {
      console.log(_item);
      return <div className="card p-2"> {_item.title} </div>;
    });
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleSubmit = (event) => {
    event.preventDefault();
    this.submitForm();
  };

  submitForm = async () => {
    console.log({ state: this.state });
    const response = await axios.get("http://localhost:3001/user/test/");
    console.log(response);
  };
}
