const mongoose = require("mongoose");
require("dotenv").config();

mongoose.connect(`mongodb://${process.env.DB_HOST}/${process.env.DB_DEFAULT}`, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
const db = mongoose.connection;
db.on("error", () => {
  console.log("> error occurred from the database");
});
db.once("open", () => {
  console.log("> successfully opened the database");
});
module.exports = mongoose;
