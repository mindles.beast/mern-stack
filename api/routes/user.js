const express = require("express");
const router = express.Router();
const bcrypt = require("bcrypt");
const userModel = require("../model/user-model");

router.post("/login", async (req, res) => {
  // destructuring
  const {
    body: { email, password },
  } = req;
  // check if email and passwords are present
  if (!email || !password) {
    res.status(400).json({ err: "All parameters are required" });
    return;
  }

  // fetch data from db with same email id as provided
  userModel.find({ email }, (err, doc) => {
    if (err) res.status(500).json({ err }).end();
    else {
      if (doc.length != 0) {
        bcrypt.compare(password, doc[0].password, (err, same) => {
          if (err) {
            if (err) res.status(500).json({ err }).end();
          } else {
            if (same) {
              res.status(200).json(doc).end();
            } else {
              res.status(400).json({ err: "incorrect password" }).end();
            }
          }
        });
      } else {
        res.status(400).json({ err: "Email not registered" }).end();
      }
    }
  });
});

router.post("/register", async (req, res) => {
  // destructuring
  const {
    body: { email, password, name },
  } = req;
  // check if parameters are present
  if (!email || !password) {
    res.status(400).json({ err: "All parameters are required" });
    return;
  }
  // email validation
  if (!/\S+@\S+\.\S+/.test(email)) {
    res.status(400).json({ err: "Email is not valid" });
  }
  // new user data with encrypted password
  let userData = {
    email: email,
    password: await bcrypt.hash(password, 10),
    name: name ? name : null,
  };
  newUser = new userModel(userData);
  newUser.save((err, doc) => {
    res.send({ doc, err });
  });
});

module.exports = router;
